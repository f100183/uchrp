{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RankNTypes #-}
module CHR.Untyped (
-- * Terms with logical variables
  TermWithVars(..)
, (@@!)
-- * Guards
, Guard(..)
-- * Pure rules
, PureRule(..)
, Solution(..)
, Justification(..)
, runPureCHRLoop
-- ** Nicer syntax for rules
, (@@)
, (\\)
, (<=>)
, (<=>..)
, (==>)
, (==>..)
, priority
, staticPriority
, when
-- * Rules with monadic context
, Rule(..)
, runCHRLoop
-- * Extra passes
, ExtraPass(..)
, ExtraPassStep(..)
, PureExtraPass
, extraPassByMatching
-- * Re-exported for ease of use
, Substitutable
, Unifiable
) where

import qualified Control.Monad.Extra as M
import Data.Function (on)
import Data.Functor.Identity
import qualified Data.IntMap as M
import Data.List (permutations)
import Data.Maybe (catMaybes)
import qualified Data.Set as S
import Data.TermWithVars

-- | A representation of a Boolean guard.
class Guard g where
  trivial   :: g
  satisfies :: g -> Bool

instance Guard () where
  trivial   = ()
  satisfies = const True
instance Guard Bool where
  trivial   = True
  satisfies = id
instance Guard a => Guard [a] where
  trivial   = []
  satisfies = all satisfies

-- | A general CHR rule of the form:
--
-- > name @ priority :: given \ wanted <=> guard | body
--
-- where the execution of the rule may have side effects given by `m`.
data Rule m v o i g c = Rule { name   :: i
                             , given  :: [c]
                             , wanted :: [c]
                             , guard  :: Substitution v -> g
                             , body   :: Substitution v -> m [[c]]
                             , prio   :: Substitution v -> o }

-- | A general CHR rule of the form:
--
-- > name @ priority :: given \ wanted <=> guard | body
--
-- In CHR literature, these are called simpagation rules.
data PureRule v o i g c = PureRule { pname   :: i
                                   , pgiven  :: [c]
                                   , pwanted :: [c]
                                   , pguard  :: Substitution v -> g
                                   , pbody   :: Substitution v -> [[c]]
                                   , pprio   :: Substitution v -> o}

pureToCHR :: PureRule v o i g c -> Rule Identity v o i g c
pureToCHR PureRule { .. } = Rule { name   = pname
                                 , given  = pgiven
                                 , wanted = pwanted
                                 , guard  = pguard
                                 , body   = return . pbody
                                 , prio   = pprio }

ruleLengths :: Rule m v o i g c -> (Int, Int)
ruleLengths r = (length $ given r, length $ wanted r)

-- | Extra pass for the entire set of constraints.
data ExtraPassStep  ix c = ExtraPassStep { from :: [ix], to :: [c] }
data ExtraPass    m nm c = ExtraPass     nm (forall ix. [(ix, c)] -> m [ExtraPassStep ix c])
data PureExtraPass  nm c = PureExtraPass nm (forall ix. [(ix, c)] ->   [ExtraPassStep ix c])

pureToExtraPass :: PureExtraPass nm c -> ExtraPass Identity nm c
pureToExtraPass (PureExtraPass nm p) = ExtraPass nm (\cs -> return $ p cs)

extraPassByMatching :: (Monad m) => nm -> (c -> m (Maybe [c])) -> ExtraPass m nm c
extraPassByMatching nm perelt = ExtraPass nm $ \constraints ->
  catMaybes <$> M.forM constraints (\(ix, c) -> do r <- perelt c
                                                   return $ case r of
                                                     Nothing  -> Nothing
                                                     Just new -> Just (ExtraPassStep [ix] new))

data Solution i c = Solution { justifications :: [Justification i c]
                             , remaining      :: [c] }
                  deriving Show

-- | Justifications tell how a given constraint was solved.
data Justification i c = Unsolved c
                       | Step i [Justification i c] [Justification i c]
                       deriving (Show, Functor)

instance Applicative (Justification i) where
  pure  = Unsolved
  (<*>) = M.ap

instance Monad (Justification i) where
  return = pure
  (Unsolved c)   >>= f = f c
  (Step i gs rs) >>= f = Step i (map (>>= f) gs) (map (>>= f) rs)

runCHRLoop :: forall m v o i g c.
              (Monad m, Ord v, Ord o, Unifiable v c, Guard g)
           => [Rule m v o i g c] -> [ExtraPass m i c] -> [c] -> m [Solution i c]
runCHRLoop rs es cs = go (initCHRState cs rs)
  where go :: CHRExecState m o i c -> m [Solution i c]
        go s@(CHRExecState { priorityTree = ptree })
           | S.null ptree
           = do s'@(CHRExecState { .. }) <- applyExtraPasses rs es s
                if S.null priorityTree
                   then let finalJustifications = (\c -> Unsolved (constraintStore M.! c))
                         in return . return $ Solution (map (>>= finalJustifications) idJustif)
                                                       (M.elems constraintStore)
                   else go s'
           | otherwise
           = do let (RuleApplication { .. }, newPrio) = S.deleteFindMin ptree
                possibleWorlds <- appliedBody
                let nextStates = do -- Get the new constraints
                                    newConstraints <- possibleWorlds
                                    -- Update the state
                                    return $ updateCHRState rs (s { priorityTree = newPrio })
                                                            appliedName appliedGiven
                                                            appliedWanted newConstraints
                -- Continue one step more
                M.concatMapM go nextStates

updateCHRState :: (Monad m, Ord v, Ord o, Unifiable v c, Guard g)
               => [Rule m v o i g c] -> CHRExecState m o i c
               -> i -> [ConstraintIdent] -> [ConstraintIdent] -> [c]
               -> CHRExecState m o i c
updateCHRState rs s@(CHRExecState { .. }) nm gvn rem ins =
  let dState = foldr deleteConstraint s rem
      (inserteds, iState) = insertConstraints rs ins dState
      -- Get the new justifications
      updJustifications = (\c -> if c `elem` rem
                                    then Step nm (map Unsolved gvn) (map Unsolved inserteds)
                                    else Unsolved c)
   in iState { idJustif = map (>>= updJustifications) idJustif }

applyExtraPasses :: forall m v o i g c.
                    (Monad m, Ord v, Ord o, Unifiable v c, Guard g)
                 => [Rule m v o i g c] -> [ExtraPass m i c]
                 -> CHRExecState m o i c -> m (CHRExecState m o i c)
applyExtraPasses _  []     s = return s
applyExtraPasses rs (e:es) s = applyExtraPass s e >>= \e' -> case e' of
                                 Just s' -> return s'
                                 Nothing -> applyExtraPasses rs es s
  where applyExtraPass :: CHRExecState m o i c -> ExtraPass m i c -> m (Maybe (CHRExecState m o i c))
        applyExtraPass s (ExtraPass nm p) = p (M.toList $ constraintStore s) >>= \result -> case result of
                                               [] -> return Nothing
                                               steps -> return $ Just (updateSteps nm steps s)
        updateSteps _  [] s = s
        updateSteps nm (ExtraPassStep rem ins : r) s = updateSteps nm r (updateCHRState rs s nm [] rem ins)

runPureCHRLoop :: (Ord v, Ord o, Unifiable v c, Guard g)
               => [PureRule v o i g c] -> [PureExtraPass i c] -> [c] -> [Solution i c]
runPureCHRLoop rs es cs = runIdentity $ runCHRLoop (map pureToCHR rs) (map pureToExtraPass es) cs

-- RULE BUILDERS
-- =============

data Named i x = Named i x
infixl 7 @@
(@@) :: i -> x -> Named i x
(@@) = Named

data Simpagation c = Simpagation [c] [c]
infixl 8 \\
-- | Defines a pair of simplified and propagated constraints.
(\\) :: [c] -> [c] -> Simpagation c
(\\) = Simpagation

infixl 6 <=>, <=>..
-- | Used to define both simplification and simpagation rules.
class Iff c t | t -> c where
  (<=>..) :: (Num o, Guard g)
          => Named i t -> (Substitution v -> [[c]]) -> PureRule v o i g c
  (<=>) :: (Num o, Guard g)
        => Named i t -> (Substitution v -> [c]) -> PureRule v o i g c
  n <=> cs = n <=>.. (\v -> [cs v])
instance Iff c [c] where
  (Named n ws) <=>.. b = PureRule {
    pname = n, pgiven = [], pwanted = ws, pguard = \_ -> trivial, pbody = b, pprio = \_ -> 0 }
instance Iff c (Simpagation c) where
  (Named n (Simpagation gs ws)) <=>.. b = PureRule {
    pname = n, pgiven = gs, pwanted = ws, pguard = \_ -> trivial, pbody = b, pprio = \_ -> 0 }

infixl 6 ==>, ==>..
-- | Defines a propagation rule with back-tracking.
(==>..) :: (Num o, Guard g)
        => Named i [c] -> (Substitution v -> [[c]]) -> PureRule v o i g c
(Named n gs) ==>.. b = PureRule {
  pname = n, pgiven = gs, pwanted = [], pguard = \_ -> trivial, pbody = b, pprio = \_ -> 0 }
 -- | Defines a propagation rule without back-tracking.
(==>) :: (Num o, Guard g)
      => Named i [c] -> (Substitution v -> [c]) -> PureRule v o i g c
n ==> cs = n ==>.. (\v -> [cs v])

infixl 5 `priority`
-- | Defines the priority of a rule to a value based on the matched constraints.
priority :: PureRule v a i g c -> (Substitution v -> o) -> PureRule v o i g c
priority r p = r { pprio = p }

infixl 5 `staticPriority`
-- | Defines a static priority for a rule.
staticPriority :: PureRule v a i g c -> o -> PureRule v o i g c
staticPriority r p = r { pprio = \_ -> p }

infixl 5 `when`
-- | Adds a guard to a CHR rule.
when :: PureRule v o i g c -> (Substitution v -> g) -> PureRule v o i g c
when r g = r { pguard = g }

-- DEFINITIONS NEEDED FOR CHR EXECUTION
-- ====================================

type ConstraintIdent = Int
data ConstraintWithIdent c = C { ident :: ConstraintIdent, c :: c }

instance Eq (ConstraintWithIdent c) where
  (==) = (==) `on` ident
instance Ord (ConstraintWithIdent c) where
  compare = compare `on` ident

data RuleApplication m o i c = RuleApplication {
    appliedName     :: i
  , appliedGiven    :: [ConstraintIdent]
  , appliedWanted   :: [ConstraintIdent]
  , appliedBody     :: m [[c]]
  , appliedPriority :: o }

instance (Show i, Show o, Show c) => Show (RuleApplication m o i c) where
  show RuleApplication { .. } = show appliedName ++ " @ " ++ show appliedPriority
                                ++ " :: " ++ show appliedGiven ++ " \\ " ++ show appliedWanted

affectedIdents :: RuleApplication m o i c -> S.Set (ConstraintIdent)
affectedIdents c = S.fromList (appliedGiven c) `S.union` S.fromList (appliedWanted c)

instance Eq (RuleApplication m o i c) where
  (==) = (==) `on` affectedIdents
-- Compare by priority, and then by affected ids
-- (older constraints will be used before)
instance Ord o => Ord (RuleApplication m o i c) where
  compare x y
    | appliedPriority x /= appliedPriority y
    = compare (appliedPriority x) (appliedPriority y)
    | otherwise
    = compare (affectedIdents x) (affectedIdents y)

data CHRExecState m o i c = CHRExecState {
    priorityTree    :: S.Set (RuleApplication m o i c)
  , constraintStore :: M.IntMap c
  , idJustif        :: [Justification i ConstraintIdent]
  , nextIdent       :: ConstraintIdent }
  deriving Show

emptyCHRState :: CHRExecState m o i c
emptyCHRState = CHRExecState {
    priorityTree    = S.empty
  , constraintStore = M.empty
  , idJustif        = []
  , nextIdent       = 0 }

initCHRState :: (Ord v, Unifiable v c, Ord o, Guard g)
             => [c] -> [Rule m v o i g c] -> CHRExecState m o i c
initCHRState cs rs
  = let (initialKeys, initialState) = insertConstraints rs cs emptyCHRState
     in initialState { idJustif = map Unsolved initialKeys }

deleteConstraint :: ConstraintIdent -> CHRExecState m o i c -> CHRExecState m o i c
deleteConstraint ident s@(CHRExecState { priorityTree, constraintStore }) = s {
    priorityTree    = S.filter (\app -> ident `S.notMember` affectedIdents app) priorityTree
  , constraintStore = M.delete ident constraintStore }

insertConstraints :: (Ord v, Unifiable v c, Ord o, Guard g)
                  => [Rule m v o i g c] -> [c]
                  -> CHRExecState m o i c -> ([ConstraintIdent], CHRExecState m o i c)
insertConstraints rs cs s = foldr (\c (inserteds, currentState) ->
                                      let (i,newState) = insertConstraint rs c currentState
                                       in (i:inserteds, newState) )
                                  ([], s) cs

insertConstraint :: (Ord v, Unifiable v c, Ord o, Guard g)
                 => [Rule m v o i g c] -> c
                 -> CHRExecState m o i c -> (ConstraintIdent, CHRExecState m o i c)
insertConstraint rs c s@(CHRExecState { .. }) = (nextIdent, s {
    priorityTree    = S.union priorityTree (S.fromList applications)
  , constraintStore = M.insert nextIdent c constraintStore
  , nextIdent       = nextIdent + 1 })
  where applications = concatMap (applyPermutedRule c nextIdent constraintStore) rs

applyPermutedRule :: (Ord v, Unifiable v c, Guard g)
                  => c -> ConstraintIdent -> M.IntMap c
                  -> Rule m v o i g c
                  -> [RuleApplication m o i c]
applyPermutedRule c nIdent store r =
  let (gLength, wLength) = ruleLengths r
   in if gLength + wLength < 1
         then []
         else catMaybes $ do
                 combination <- M.toList <$> combinations (gLength+wLength-1) store
                 let combinationWithC = (nIdent,c) : combination
                 permutation <- permutations combinationWithC
                 let (gs, ws) = splitAt gLength permutation
                 return $ applyRule r gs ws

applyRule :: (Ord v, Unifiable v c, Guard g)
          => Rule m v o i g c
          -> [(ConstraintIdent,c)] -> [(ConstraintIdent,c)]
          -> Maybe (RuleApplication m o i c)
applyRule r gs ws = do
  ss <- unify (given r, wanted r) (snd <$> gs, snd <$> ws)
  M.guard $ satisfies (guard r ss)
  return $ RuleApplication (name r) (fst <$> gs) (fst <$> ws) (body r ss) (prio r ss)

combinations :: Int -> M.IntMap a -> [M.IntMap a]
combinations n s
  | n <= 0    = [M.empty]
  | M.null s  = []
  | otherwise = let ((k,x), s') = M.deleteFindMin s
                 in (M.insert k x <$> combinations (n-1) s') ++ combinations n s'
