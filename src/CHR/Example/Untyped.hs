{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module CHR.Example.Untyped where

import GHC.Generics

import CHR.Untyped

data LVar = X | Y | Z | A deriving (Eq, Ord, Show)

data LeqConstraint = Leq (TermWithVars LVar String) (TermWithVars LVar String)
                   | Eq  String String
                   deriving (Show, Generic)

instance Substitutable LVar LeqConstraint
instance Unifiable LVar LeqConstraint

data LeqRule = Reflexivity | Antisymmetry | Idempotence | Transitivity
             deriving Show

leqRules :: [PureRule LVar Integer LeqRule () LeqConstraint]
leqRules = [reflexivity, antisymmetry, idempotence, transitivity]

reflexivity, antisymmetry, idempotence, transitivity :: PureRule LVar Integer LeqRule () LeqConstraint
reflexivity  = Reflexivity  @@
  [Leq (Var X) (Var X)] <=> const []
antisymmetry = Antisymmetry @@
  [Leq (Var X) (Var Y), Leq (Var Y) (Var X)] <=> \s ->
    [Eq (unTerm $ (s @@! X :: TermWithVars LVar String))
        (unTerm $ (s @@! Y :: TermWithVars LVar String))]
idempotence  = Idempotence @@
  [Leq (Var X) (Var Y)] \\ [Leq (Var X) (Var Y)] <=> const []
transitivity = Transitivity @@
  [Leq (Var X) (Var Y), Leq (Var Y) (Var Z)] ==> (\s ->
    [Leq (s @@! X) (s @@! Z)]) `staticPriority` 2

exampleLeqConstraints :: [LeqConstraint]
exampleLeqConstraints = [ Leq (Term "a") (Term "b")
                        , Leq (Term "b") (Term "c")
                        , Leq (Term "c") (Term "a") ]

data DijkstraConstraint = Edge (TermWithVars LVar String) (TermWithVars LVar Int) (TermWithVars LVar String)
                        | Source (TermWithVars LVar String)
                        | Dist (TermWithVars LVar String) (TermWithVars LVar Int)
                        deriving (Show, Generic)

instance Substitutable LVar DijkstraConstraint
instance Unifiable LVar DijkstraConstraint

dijkstraRules :: [PureRule LVar Int String Bool DijkstraConstraint]
dijkstraRules = [start, remove, prop]

start, remove, prop :: PureRule LVar Int String Bool DijkstraConstraint
start  = "start" @@ [Source (Var X)] ==> (\s -> [Dist (s @@! X) (Term 0)]) `staticPriority` 1
remove = "remove" @@ [Dist (Var X) (Var Y)] \\ [Dist (Var X) (Var Z)] <=> const []
           `when` (\s -> (unTerm (s @@! Y :: TermWithVars LVar Int)) <= (unTerm (s @@! Z :: TermWithVars LVar Int)))
           `staticPriority` 1
prop   = "prop" @@ [Dist (Var X) (Var Y), Edge (Var X) (Var A) (Var Z)] ==> (\s ->
           let m = (unTerm (s @@! Y :: TermWithVars LVar Int)) + (unTerm (s @@! A :: TermWithVars LVar Int))
            in [Dist (s @@! Z) (Term m)])
           `priority` (\s -> (unTerm (s @@! Y :: TermWithVars LVar Int)) + 2)

exampleDijsktraConstraints :: [DijkstraConstraint]
exampleDijsktraConstraints = [ Edge (Term "a") (Term 1) (Term "b")
                             , Edge (Term "b") (Term 3) (Term "d")
                             , Edge (Term "a") (Term 2) (Term "c")
                             , Edge (Term "c") (Term 1) (Term "d")
                             , Source (Term "a")]
